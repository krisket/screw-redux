import React, { useContext } from 'react';
import { AppContext } from '../../App';

export default () => {
  const { state, dispatch } = useContext(AppContext);

  return (
    <div>
      <h2>{state.title}</h2>
      <div className="btn" style={{ marginLeft: '1em' }}
        onClick={() => { dispatch({ type: 'update-title', title: `${state.title} - Agreed.` }) }}>Click Here to agree.</div>
    </div>
  );
};