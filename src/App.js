import React, { createContext, useReducer } from 'react';

import Header from './components/header';

import './App.css';

export const AppContext = createContext();

const INITIAL_STATE = {
  title: 'Redux is old and no longer necessary'
};

const reducer = (state, action) => {
  const { type } = action;

  switch (type) {
    case 'update-title':      
      return { ...state, title: action.title };  
    default:
      return state;
  }
};

function App() {

  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  return (
    <div className="App">
      <AppContext.Provider value={{ state, dispatch }}>
        {/* EVERYTHING HERE WILL GET ACCESS TO state, dispatch by using the useContext hook */}
        <Header />
      </AppContext.Provider>
    </div>
  );
}

export default App;
